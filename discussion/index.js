//Javscript Synchronous vs Asynchronous

// Synchronous JS - default behavior; only one  statement can be performed at a time and only if one is completed, the preceding is unblocked, otherwise it will not proceed;
	//blocking - additional js process execution must wait until the operation is completed.

console.log('Hello World')
console.log('Hello Again')

/*for (let i = 0; i <= 15; i++) {
	console.log(i)
}*/
console.log('Goodbye')



//Asynchronous JS
// Asynchronous - exexcuting at the sametime
	//most asynch js operations can be classified through:
		// Browser/WEB API events or functions; include methods like setTimeout, ir evebt handlkers like onclicke, mouse over scroll, etc/;

		//promises - unique js object that allows performing asynch behavior; promises object represents the eventual completion or failure of an asynch operation and its resulting value

//BROWSER/WEB API*********************
/*function printMe(){
	console.log('print Me');
}
setTimeout(printMe, 5000)
*/
/*
function prints() {
	console.log('print meeeeee');
}
function test() {
	console.log('test');
}
setTimeout(prints, 4000)
test()*/


function f1() {
	console.log('f1');
}
function f2() {
	console.log('f2');
}
function main() {
	console.log('main');

	setTimeout(f1,0) //callback queue

	//PROMISE****** (job queue - more priority over the callback queue	)
	new Promise((resolve, reject)=>
			resolve('I am a promise')
		).then(resolve => console.log(resolve))

	f2()
}
main()

//REST API using JSON Placeholder*******************************


// GETTING ALL POSTS (GET Method)
	//fetch - allows requesting for a rescource asynchronously
	//promise is used for asynch; maybe 1 of 3 possible states (pending, fulfilled, rejected)
	/*
		fetch ('url', {optional obejcts}).then (response => response.json())
		.then(data)
	*/

		console.log(fetch ('https://jsonplaceholder.typicode.com/posts'))

		fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()) //parse the responses as JSON
		.then(data =>{
			console.log(data)
		}) //process the results
/*
async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result)
	console.log(typeof result)

	//converting data from the response
	let json = await result.json()
	console.log(json)
	console.log(typeof json)
}

fetchData()*/


//CREATES A NEW POST*******************
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			//sets the content of the REQUEST
			body: JSON.stringify({
				title: 'New Post',
				body: 'Hello Again',
				userId: 2
			})
		}).then(response => response.json()) 
		.then(data =>{
			console.log(data)
		}) 

//UPDATING*************
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PUT',
			headers:{
				'Content-Type': 'application/json'
			},
			//sets the content of the REQUEST
			body: JSON.stringify({
				title: "New Post",
				body: "Hello Again",
				userId: 2
			})
		}).then(response => response.json()) 
		.then(data =>{
			console.log(data)
		}) 

//DELETE A POST******************
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'DELETE'}).then(response => response.json()) 
		.then(data =>{
			console.log(data)
		}) 

//filtering posts
	// adding ? will signal the filtering of data
	/*
	********for Individual Parameter
		<url>?parameterName=<value>
	********for Multiple Parameters
		<url>?parameterNameA=<valueA>&parameterNameB=<valueB>
	*/
		fetch('https://jsonplaceholder.typicode.com/posts?userId=1').then(response => response.json()) 
		.then(data =>{
			console.log(data)
		}) 


//retrieving a nested comments to posts

	//for a specific post (<url>/<id>/comments)
		fetch('https://jsonplaceholder.typicode.com/posts/2/comments').then(response => response.json()) 
		.then(data =>{
			console.log(data)
		}) 
