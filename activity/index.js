
// FETCH GET + MAP ***************
		fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()) 
		.then(data =>{
			console.log(data)
			let newArr = data.map(function(title){
				return `${title.title}`
			})

			console.log(newArr)
		}) 



// FETCH REQUEST-GET***********
fetch('https://jsonplaceholder.typicode.com/todos/1').then(response => response.json())
		.then(data =>{
			console.log(data)
			console.log(data.title)
			console.log(data.completed)
		})



//FETCH-REQUEST-POST***********
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New To Do Post',
		completed: true,
		userId: 2
	})
}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 



//FETCH REQUEST-PUT***********
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New To Do Put',
		completed: true,
		userId: 2
	})
}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 



//FETCH REQUEST-PUT-PROPERTY-CHANGE***********
fetch('https://jsonplaceholder.typicode.com/todos/200', {
	method: 'PUT',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New To Do Put',
		description: "new to do list item",
		userId: 2,
		status: "completed",
		date_completed: "last night"
	})
}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 



//FETCH REQUEST-PATCH***********
fetch('https://jsonplaceholder.typicode.com/todos/11', {
	method: 'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New To Do Patch',
		completed: true,
		userId: 2
	})
}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 


//FETCH REQUEST-PATCH-COMPLETE+DATE-COMPLETED***********
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		date_completed: "tomorrow"
	})
}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 


//FETCH-REQUEST-DELETE***********
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'}).then(response => response.json()) 
.then(data =>{
	console.log(data)
}) 
